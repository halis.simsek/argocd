 #!/bin/bash

pwd

cd src/main/resources

set -e
export IMAGE_NAME=$(grep 'image.name=' application.properties | awk -F'=' '{ gsub(/ /,"");print $2 }')
export IMAGE_VERSION=$(grep 'image.version=' application.properties | awk -F'=' '{ gsub(/ /,"");print $2 }')

echo $IMAGE_NAME
